#!/usr/bin/env python

from flask import Flask, render_template, flash, redirect, session, request, send_from_directory, jsonify
import jinja2
import os
from flask_bootstrap import Bootstrap
from flask.ext.wtf import Form
from wtforms import TextField, HiddenField, ValidationError, SelectField,\
    BooleanField, SubmitField, FloatField, FormField, validators
from wtforms.validators import Required, Regexp, Optional, NumberRange
import pandas as pd
import numpy as np
from MnMfunction import MnM, getRackett
import datetime
from tunedCompounds import tunedComps

class CompoundForm(Form):
    cas = TextField('CAS number', description='E.g. 111-83-1', 
              validators=[Regexp("^\d{2,7}-\d\d-\d$",message="Not a valid CAS"),Optional()])
    formula = TextField('Molecular formula', description='E.g. C8H17Br',
              validators=[Regexp("^([A-Z][a-z]?[0-9]*)+$",message="Not a valid chemical formula"),Optional()])
    name = TextField('Name or part(s) of a name', description=\
                "Search for an exact match by putting an exclamation mark first, e.g. \
                \"!silane\" will match only silane and not methyl silane. Spaces separating \
                words are interpreted as logical AND, e.g. \"methyl silane\" searches for all \
                names containing \"methyl\" AND \"silane\". \"!methyl silane\" will do an \
                exact match on \"methyl silane\". Only letters, numbers, dash, comma, \
                exclamation mark and space are allowed symbols for security reasons.", \
                validators=[Regexp("^!*[-,A-Za-z0-9 ]+$",message="Name contains illegal symbols"),Optional()]) 
    Lookup = SubmitField('Look up this compound')

class RefineForm(Form):
    rho07 = FloatField('Density at 0.7 * Tc?',
            description='Input just a number, in mol/m<sup>3</sup>',
            validators=[Optional()])
    reference = TextField('Bibliographic reference?', 
            description='E.g. \"R. Span & W. Wagner (1996). J. Phys. Chem. Ref. Data, 25(6), 1509-1596.\"',
            validators=[Optional()]) 
    submit = SubmitField('Refine')
    skip = SubmitField('Skip refinement')

class MnMForm(Form):
    name = TextField('Name of the compound (optional)?', 
            description='E.g. methyl bromide',
            validators=[Optional()]) 
    Tc = FloatField('Critical temperature',
            description='Input just a number, in K',
            validators=[Required()])
    Pc = FloatField('Critical pressure',
            description='Input just a number, in bar',
            validators=[Required()])
    rhoc = FloatField('Critical density',
            description='Input just a number, in mol/m<sup>3</sup>',
            validators=[Required()])
    popover_omega = \
    """
        <button type="button" class="btn btn-xs btn-info" data-toggle="popover"
        title="Acentric factor" 
        data-content=
            "The acentric factor is a measure of
            the non-sphericity of a molecule, computed from the logarithm of the vapor
            pressure at a reduced temperature of 0.7. <br><a
            href=\'https://en.wikipedia.org/wiki/Acentric_factor\'>Wikipedia has
            more.</a>"
        >What is the acentric factor?</button>
   """
    omega = FloatField('Pitzer\'s acentric factor (omega)',
            description='Input just a number'+popover_omega,
            validators=[Required()])
    rho07 = FloatField('Liquid density at 0.7 * Tc',
            description='Input just a number, in mol/m<sup>3</sup>',
            validators=[Optional()])
    Rackett = BooleanField("Use the Rackett equation for liquid density?",
            validators=[Optional()])
    reference = TextField('Bibliographic reference (optional)',
            description='E.g. \"R. Span & W. Wagner (1996). J. Phys. Chem. Ref. Data, 25(6), 1509-1596.\"',
            validators=[Optional()]) 
    submit = SubmitField('Submit')

class SelectForm(Form):
    theone = SelectField('Which one do you want?',coerce=int,validators=[Required()])
    Lookup = SubmitField('Submit')


# Import the dataset into a dataframe
#df = pd.read_hdf('Yaws-MnM-production.h5','table')
df = pd.read_pickle('Yaws-MnM-production.pickle')

# Start the webapp
app = Flask(__name__)

# Set config
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'not_a_secret'

# Custom loader, so we can include .bib file from static/ in templates
my_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(os.path.join(os.curdir, 'static'))
    ])
app.jinja_loader = my_loader

# Bootstrap it
Bootstrap(app)

def saveToStore(frame):
    pass
#    # Import the store
#    a = datetime.datetime.now()
#    user_data_store = pd.HDFStore('user_provided_data.h5',mode="a")
#    # Reindex the frame so it can be appended to the store
#    try:
#        nrows = user_data_store.get_storer('table').nrows
#    except:
#        nrows = 0
#    frame.index = pd.Series(frame.index) + nrows
#    user_data_store.append('table',frame)
#    user_data_store.close()
#    b = datetime.datetime.now()
#    delta = b-a
#    print("Time for HDFStore: "+str(int(delta.total_seconds()*1e6))+" microseconds.")

# Now define all our pages
@app.route('/index')
@app.route('/')
def index():
    return render_template('index.html', form=None)

@app.route('/bottlesearch', methods=('GET','POST'))
def bottlesearch():
    session.clear()
    form = CompoundForm(request.form,csrf_enabled=False)
    if request.method == 'POST' and form.validate():
        # Set the result to blank, this will be filled in later if search was successful
        session['result_compound'] = ""
        resultFrame = pd.DataFrame()
        # This will be set to True if the search returns too many results
        session['results_toomany'] = False
        # Check if the user has provided a CAS
        if form.cas.data != "":
            # If so, get the dataframe with this CAS. If this result is
            # empty, we try the name and then the formula.
            resultFrame = df[df['CAS'].values == form.cas.data]
        # Check if user has provided a name search and that we haven't
        # already got the compound from the CAS
        # First, split off any leading or trailing spaces
        name = form.name.data.lstrip().rstrip()
        if name != "" and len(resultFrame) == 0:
            # If user has requested exact match, do that
            if name.startswith("!"):
                # Remove the ! in the start of the string
                name = name[1:]
                # Look it up
                resultFrame = df[df['name'].values == name]
            else: # do a substring search
                # Split into list of words
                words = name.split()
                # Pop the last word from the list
                wl = words.pop()
                # First create resultFrame where name contains the last word
                #resultFrame = df[df['name'].str.contains(wl)]
                # Crazy optimization of above line after profiling, 5x (!) faster
                resultFrame = df[ np.array([wl in s for s in df["name"].values]) ]
                # Then loop over remaining words
                for word in words:
                    resultFrame = resultFrame[resultFrame['name'].str.contains(word)]
            # If we didn't find it, we try finally the formula
        # If none of the above were given or successful, lookup formula
        if len(resultFrame) == 0:
            resultFrame = df[df['formula'].values == form.formula.data]
        # How many matches did we get?
        nMatches = len(resultFrame['name'].unique())
        # If we ended up with too many results, limit to the first 20
        if  nMatches > 20:
            # Get the number of compounds found
            session['results_number'] = nMatches
            # Get the first 6 unique names
            names = resultFrame['name'].head(120).unique().tolist()[0:19]
            # Then restrict to just compounds with this name
            resultFrame = resultFrame[resultFrame['name'].isin(names)]
            session['results_toomany'] = True
        session.modified = True
        # If the result is still empty, we failed!
        if nMatches == 0:
            return redirect('/notfound')
        else: 
            # If we ended up with just one compound, offer to refine it
            if nMatches == 1:
                # Set the resulting compound into a dataframe
                session['result_compound'] = resultFrame.to_json(double_precision=15)
                return redirect('/refine')
            else: # let the user restrict the selection
                # Set the matching indices into a session variable and return it
                # This way we can fit many more compounds into a string
                session['result_indices'] = resultFrame['CAS'].index.values.tolist()
                return redirect('/restrict')
    return render_template('bottlesearch.html', form=form)

@app.route('/restrict', methods=('GET','POST'))
def restrict():
    # Get the result from the indices stored in a session variable
    resultFrame = df.ix[session['result_indices']]
    # Get a list of the names of the compounds
    nameList = resultFrame['name'].unique().tolist()
    # Set up the form that lets the user choose one of these
    form = SelectForm(request.form,csrf_enabled=False)
    form.theone.choices = zip(range(len(nameList)),nameList)
    # Tell the user if we had to limit the number of returned results
    if session['results_toomany']:
        session['result_restrict_status'] = "We found "+str(session['results_number'])+" results! Limiting to the first 20."
        session['offer_retry'] = '<a href="bottlesearch" class="btn btn-primary" role="button">Go back and search again</a>'
    else:
        session['result_restrict_status'] = "We found several compounds matching your search."
        session['offer_retry'] = ''
    if request.method == 'POST':
        # User has selected one from the dropdown list. Give them that.
        name = nameList[form.theone.data]
        resultFrame = resultFrame[resultFrame['name'] == name]
        session['result_compound'] = resultFrame.to_json(double_precision=15)
        session.modified = True
        return redirect('/refine')
    return render_template('restrict.html', form=form)

@app.route('/refine', methods=('GET','POST'))
def refine():
    # Get the result from the JSON representation stored in session variable
    resultFrame = pd.read_json(session['result_compound'],precise_float=True)
    # Put things into session variables for showing the user
    session['result_name'] = resultFrame['name'].unique().tolist()[0]
    session['result_formula'] = resultFrame['formula'].unique().tolist()[0]
    session['Tcrit07'] = "{0:.2f}".format(resultFrame['Tcrit07'].unique().tolist()[0])
    session['rho07'] = "{0:.2f}".format(resultFrame['rho_Rackett'].unique().tolist()[0])
    # Create a URL for showing a picture of the thing
    # First check that we have a CAS number for it. Default value is blank (no
    # image). Tried checking whether NIH has a picture for the CAS, but it's too slow.
    theCAS = resultFrame['CAS'].unique().tolist()[0]
    session['result_img'] = ""
    if not pd.isnull(theCAS):
        img_url = "http://cactus.nci.nih.gov/chemical/structure/"+theCAS+"/image"
        session['result_img'] = img_url
    # Check if the requested compound is something for which we can do better 
    # than the MnM, if so redirect the user
    if theCAS in tunedComps:
        return redirect('/betteravailable')
    # Else, show the user the form
    form = RefineForm(request.form,csrf_enabled=False)
    if request.method == 'POST' and form.validate():
        if form.submit.data and form.rho07.data != None:
            # This means user provided a better density
            # Get the user-supplied liquid density at Tr = 0.7
            rho07 = form.rho07.data
            # Modify resultFrame with it
            resultFrame['rho_Rackett'] = rho07
            # Compute the sigma (the only thing that changes) for the new rho07
            resultFrame['sigma'] = resultFrame[['M21','rho_Rackett']].apply( lambda rf: (rf[0]/(rf[1]*6.0221367E+23))**(1/3), axis=1 )
            # Then store the given density if source is given
            if form.reference.data != "":
                # Create a dataframe with this result to store
                storeFrame = resultFrame
                storeFrame['ref'] = form.reference.data
                saveToStore(storeFrame)
            session['refined'] = True
        else:
            # Return data with Rackett density 
            session['refined'] = False
        # Either way, return the result
        session['result_compound'] = resultFrame.to_json(double_precision=15)
        session.modified = True
        return redirect('/result')
    return render_template('refine.html', form=form)

@app.route('/result')
def result():
    # Get the result from the JSON representation stored in session variable 
    resultFrame = pd.read_json(session['result_compound'],precise_float=True) 
    # Make a string reminding the user whether this was with a refined value
    # of liquid density 
    if session['refined']:
        session['result_imp_status'] = "provided by you as a more accurate value"
    else:
        session['result_imp_status'] = "estimated from the Rackett equation"
    # Boolean for the HTML to know if there was a good result for more than
    # one value of m
    session['result_several_rows'] = len(resultFrame.index) > 1
    # One-liner dummy object for holding the compound properties. Looks incomprehensible, I know.
    properties = type('',(),{})()
    properties.CAS = resultFrame['CAS'].unique().tolist()[0]
    properties.formula = resultFrame['formula'].unique().tolist()[0]
    properties.mass = '{0:6.2f}'.format(resultFrame['molmass'].unique().tolist()[0])
    properties.rho07 = '{0:6.2f}'.format(resultFrame['rho_Rackett'].unique().tolist()[0])
    properties.name = resultFrame['name'].unique().tolist()[0]
    # Dicts for holding parameters and raaSAFT scripts
    parameters = dict()
    raasaft = dict()
    # Set the current date for output in script file
    gendate = datetime.date.today().strftime("%d. %b. %Y")
    for i, row in resultFrame.iterrows():
        # Dummy object for holding parameters per compound
        comp = type('',(),{})()
        comp.name = ''.join([i for i in properties.name if i.isalpha()])
        # Format floating point numbers to the correct precision
        comp.epsilon = '{0:6.2f}'.format(row['epsilon'])
        comp.sigma = '{0:6.4e}'.format(row['sigma'])
        comp.lambda_a = '{0:6.3f}'.format(row['lambda_r'])
        comp.lambda_r = row['lambda_a']
        comp.segments = row['m']
        comp.mass = properties.mass
        comp.rho07 = properties.rho07
        # Put it in the parameters dict
        parameters[comp.segments] = comp
        # Rescale sigma for the units used in raaSAFT
        comp.scaledsigma = '{0:6.4f}'.format(row['sigma'] * 1e10)
        # Render the raaSAFT script template and put it in the other dict
        raasaft[comp.segments] = render_template("raasaft-comp.py", comp=comp, gendate=gendate)
    # Render the page
    session.modified = True
    return render_template('result.html', form=None, properties=properties, parameters=parameters, raasaft=raasaft)

@app.route('/custom', methods=('GET','POST'))
def custom():
    form = MnMForm(request.form,csrf_enabled=False)
    session.clear()
    if request.method == 'POST' and form.validate():
        # Get the user-supplied properties
        name = form.name.data
        if name == "":
            name = "unnamed"
        session['custom_name'] = name
        reference = form.reference.data
        Tc = form.Tc.data
        Pc = form.Pc.data
        rhoc = form.rhoc.data
        omega = form.omega.data
        rho07 = form.rho07.data
        Rackett = form.Rackett.data
        # Create the "header part" of the dataframe
        spec = {"name":name,"critical temperature (K)":Tc,\
                "critical pressure (bar)":Pc,"critical density (mol/m^3)":rhoc,\
                "acentric factor":omega,"Liquid density @ T = 0.7 Tc (mol/m^3)":rho07}
        # If user wants to use the Rackett equation, do that
        if Rackett:
            session['custom_use_rackett'] = True
            rho07 = getRackett(rhoc,Tc,Pc)
            spec['Liquid density @ T = 0.7 Tc (mol/m^3)'] = rho07
        else:
            session['custom_use_rackett'] = False
        # Rerun the correlation for m from 1 to 6.
        results = []
        for i in range(1,7):
            res = MnM(Tc,Pc,rhoc,omega,rho07,m=i)
            res.update(spec)
            results.append(res)
        resultFrame = pd.DataFrame(results)
        session['result_custom'] = resultFrame.to_json(double_precision=15)
        # Then store this density if source is given
        if form.reference.data != "" and name != "unnamed":
            # Create a dataframe with this result to store
            storeFrame = resultFrame
            storeFrame['ref'] = form.reference.data
            saveToStore(storeFrame)
        return redirect('/customresult')
    return render_template('custom.html', form=form)

@app.route('/customresult')
def customresult():
    # Get the result from the JSON representation stored in session variable 
    resultFrame = pd.read_json(session['result_custom'],precise_float=True)
    # Rename the columns
    resultFrame.rename(columns={"m":"# segments","epsilon":"epsilon (K)",\
            "sigma":"sigma (m)"},inplace=True)
    # Make a HTML table with the compound name and properties
    session['custom_header_html'] = resultFrame.head(1).to_html(columns=[\
            'name','critical density (mol/m^3)','critical temperature (K)', \
            'critical pressure (bar)','acentric factor','Liquid density @ T = 0.7 Tc (mol/m^3)'\
            ],index=False,classes="table"
            ).replace("dataframe ","").replace('border="1"','')
    # Make another HTML table with the parameters
    session['custom_param_html'] = resultFrame.to_html(\
            columns=['# segments','lambda_r','lambda_a','epsilon (K)','sigma (m)'],\
            index=False,classes="table table-striped"\
            ).replace("dataframe ","").replace('border="1"','')
    # Make a string reminding the user whether this was with a refined value
    # of liquid density 
    if session['custom_use_rackett']:
        session['custom_imp_status'] = "estimated from the Rackett equation"
    else:
        session['custom_imp_status'] = "provided by you"
    session.modified = True
    # Then show the result
    return render_template('customresult.html', form=None)


@app.route('/_get_molecule')
def get_molecule():
    molFile = request.args.get('molFile')
    #mol = Chem.MolFromMolBlock(molFile,sanitize=False)
    #try:
    #    Chem.SanitizeMol(mol)
    #    smol = Chem.MolToSmiles(mol)
    #except ValueError as e:
    #    smol = str(e).replace("Sanitization error","Error")
    return jsonify(result="")

@app.route('/heteronuclear')
def heteronuclear():
    return render_template('heteronuclear.html', form=None)

@app.route('/about')
def about():
    return render_template('about.html', form=None)

@app.route('/betteravailable')
def betteravailable():
    return render_template('betteravailable.html', form=None)

@app.errorhandler(404)
def not_found(error):
    return render_template('error.html'), 404

@app.route('/notfound')
def notfound():
    return render_template('notfound.html', form=None)

if __name__ == '__main__':
    app.run()
