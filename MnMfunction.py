def getRackett(rhoc,Tc,Pc):
    return rhoc*(8.31446E-05*Tc*rhoc/Pc)**0.708934437

def MnM(Tc,Pc,rhoc,omega,rho07,m):

    sigma = lambda_r = epsilon = 0.0

    if m == 1:
        mya = 14.83593084
        myb = -6.96298673
        myc = 22.20188339
        myd = 468.7358133
        mye = 7220.95995
        myf = -983.603764
        myg = 23193.47496
        myh = 914.3607833
        myi = -6207.46635
        myj = -1383.44414
        myk = 1732.959956
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3+myi*omega**4+myk*omega**5)/(1+myb*omega+myd*omega**2+myf*omega**3+myh*omega**4+myj*omega**5)
        mya = 0.128418694
        myb = 0.404864417
        myc = 1.677162694
        myd = -0.15919454
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha)/(1+myb*alpha+myd*alpha*alpha))
        mya = 1.896593618
        myb = -1.62045652
        myc = -6.98084869
        myd = -0.80187118
        mye = 10.63303009
        myf = 1.708603778
        myg = -9.20413054
        myh = -0.53329899
        myi = 4.250332513
        myj = 1.053581249
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3+myi*alpha**4)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4+myj*alpha**5)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    elif m == 2:
        mya = 8.003422132
        myb = -5.26687379
        myc = -22.511103
        myd = 10.22988602
        mye = 3.575024001
        myf = -6.48604227
        myg = 60.3129302
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3)/(1+myb*omega+myd*omega**2+myf*omega**3)
        mya = 0.112471398
        myb = -3.19537
        myc = 1.540433976
        myd = 2.517383739
        mye = -5.87688084
        myf = 0.35178742
        myg = 5.242694662
        myh = -0.16540502
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4))
        mya = -0.06956046
        myb = -10.5645885
        myc = -1.94405528
        myd = 25.49144986
        mye = 6.257546817
        myf = -20.5091442
        myg = -5.44310717
        myh = 3.67529192
        myi = 0.873127484
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3+myi*alpha**4)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    elif m == 3:
        mya = 6.98288283
        myb = -3.86904975
        myc = -13.7097069
        myd = 5.251856687
        mye = -1.96037436
        myf = -2.36371109
        myg = 17.32374946
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3)/(1+myb*omega+myd*omega**2+myf*omega**3)
        mya = -0.20924076
        myb = -1.37777443
        myc = 4.26721507
        myd = -2.48364057
        mye = -9.77031277
        myf = 3.528008069
        myg = 4.866147232
        myh = 0.791774193
        myi = -0.19503132
        myj = -0.12464336
        myk = 4.212468181
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha+mye*alpha**2+myg*alpha**3+myi*alpha**4+myk*alpha**5)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4+myj*alpha**5))
        mya = 0.06560659
        myb = -8.93086313
        myc = -1.46302137
        myd = 18.9584471
        mye = 3.699140072
        myf = -11.6668281
        myg = -2.5080772
        myh = -0.25609451
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    elif m == 4:
        mya = 6.415946724
        myb = -6.97513187
        myc = -34.3656175
        myd = 19.20628034
        mye = 59.61079835
        myf = -26.0582818
        myg = -21.6578918
        myh = 17.42217972
        myi = -35.8209965
        myj = -4.57567105
        myk = 27.23582702
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3+myi*omega**4+myk*omega**5)/(1+myb*omega+myd*omega**2+myf*omega**3+myh*omega**4+myj*omega**5)
        mya = 0.135041681
        myb = -5.85398723
        myc = 1.311532849
        myd = 13.34114852
        mye = -10.1436616
        myf = -14.3301568
        myg = 24.07289203
        myh = 6.730897463
        myi = -24.808427
        myj = -0.7830166
        myk = 9.710951926
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha+mye*alpha**2+myg*alpha**3+myi*alpha**4+myk*alpha**5)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4+myj*alpha**5))
        mya = 0.10252404
        myb = -8.10766945
        myc = -1.19478912
        myd = 16.786554
        mye = 2.844771765
        myf = -9.63544898
        myg = -1.95195179
        myh = -1.23903092
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    elif m == 5:
        mya = 6.12835725
        myb = -2.84856149
        myc = -9.15683798
        myd = 2.782770909
        mye = -0.22289564
        myf = -0.90298914
        myg = 4.5310789
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3)/(1+myb*omega+myd*omega**2+myf*omega**3)
        mya = 0.110713117
        myb = -3.13411835
        myc = 1.980666037
        myd = 2.765748576
        mye = -6.67199531
        myf = -0.27372772
        myg = 5.484068126
        myh = -0.043068
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4))
        mya = 0.110777611
        myb = -7.37489698
        myc = -0.98996221
        myd = 14.53126538
        mye = 2.218722062
        myf = -7.49666538
        myg = -1.50270742
        myh = -1.92091089
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3+myh*alpha**4)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    elif m == 6:
        mya = 5.921678461
        myb = -2.52907417
        myc = -8.07106878
        myd = 2.186427641
        mye = 0.426376247
        myf = -0.62977458
        myg = 2.559960841
        lambda_r = (mya+myc*omega+mye*omega**2+myg*omega**3)/(1+myb*omega+myd*omega**2+myf*omega**3)
        mya = 0.130226302
        myb = -3.10782289
        myc = 1.93570739
        myd = 2.805801744
        mye = -6.45905649
        myf = -0.43748638
        myg = 5.186407719
        CC = lambda_r/(lambda_r-6) * (lambda_r/6)**(6/(lambda_r-6))
        alpha  = CC*(1/3 -1/(lambda_r-3))
        epsilon = Tc/((mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3))
        mya = 0.266554267
        myb = 1.7499403
        myc = -0.42677989
        myd = -10.1370339
        mye = -0.27319773
        myf = 9.438065562
        myg = 0.648629893
        M21   = (mya+myc*alpha+mye*alpha**2+myg*alpha**3)/(1+myb*alpha+myd*alpha**2+myf*alpha**3)
        sigma = (M21/(rho07*6.0221367E+23))**(1/3)

    else:
       return "Error, not valid for m="+str(m)

    # If valid m was specified, return the results
    return {"lambda_a":6,"lambda_r":lambda_r,"epsilon":epsilon,"sigma":sigma,"m":m}
