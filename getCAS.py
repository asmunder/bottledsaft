#!/usr/bin/python
import pandas as pd

df = pd.read_hdf("Yaws-MnM-final-allsegments.h5", "table")

compounds=[
        "methane",
        "ethane",
        "propane",
        "butane",
        "pentane",
        "hexane",
        "heptane",
        "octane",
        "nonane",
        "decane",
        "undecane",
        "dodecane",
        "tetradecane",
        "hexadecane",
        "octadecane",
        "eicosane",
        "benzene",
        "toluene",
        "ethylbenzene",
        "naphthalene",
        "nitrogen",
        "carbondioxide",
        "oxygen",
        "hydrogensulphide",
        "sulphurdioxide",
        "carbonmonoxide",
        "helium",
        "argon",
        "pyridine",
        "pyrrolidine",
        "pyrrole",
        "thiolane",
        "thiophene",
        "isopentane",
        "isobutane",
        "cyclopropane",
        "cyclopentane",
        "cyclohexane",
        "ethylene",
        "propylene",
        "1-pentene",
        "1-decene",
        "water",
        "tetrahydrofuran",
        "dimethylsulfide"]
cdf = df[df['name'].isin(compounds)]
print(cdf.head(1))
gdf = cdf.groupby('name')
index = [gp_keys[0] for gp_keys in gdf.groups.values()]
udf = df.reindex(index)
mdf = udf.as_matrix(columns=['CAS No', 'name'])
print(type(udf['CAS No'].head(1).values[0]))
print(mdf)

