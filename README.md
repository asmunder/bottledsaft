#README#

This repo contains the source for the web application running at http://www.bottledsaft.org

If you wish to test the code, follow these instructions. It should work on any Linux or Mac machine.

* Ensure git, Python 3 and pip for Python 3 is installed.

* Clone the repository:  
  `git clone https://bitbucket.org/asmunder/bottledsaft.git`  
  In the default branch, the parsing of the ChemDoodle molecule drawing into
  RDkit has been commented out, as well as the user data storage; this is to
  avoid two dependencies that are more difficult to install (HDF5 and RDkit).

* Install the dependencies:  
  `sudo pip3 install flask wtforms flask-bootstrap flask-wtf pandas numpy requests`  

* Enter the folder that git cloned:  
  `cd bottledsaft`

* Run the program:  
  `python3 ./saftflask.py`  

* Open http://127.0.0.1:5000 in your web browser

### License
This Free and Open Source software is released under the MIT license.