#!/usr/bin/env python

from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from saftflask import *

settings = {"static_path": os.path.join(os.path.dirname(__file__), "static")}

if __name__ == "__main__":
    app = create_app()
    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(5000)
    IOLoop.instance().start()
