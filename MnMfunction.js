    function MMResults(mytextarea){
        
        var m     = parseInt(document.getElementById('mym').value);
        var Tc    = Number(document.getElementById('myTc').value);
        var Pc    = Number(document.getElementById('myPc').value);
        var rhoc  = Number(document.getElementById('myrhoc').value);
        var omega = Number(document.getElementById('myomega').value);
        var rho07 = Number(document.getElementById('myrho07').value);
        var omega2= omega*omega;
        var omega3= omega2*omega;
        var omega4= omega3*omega;
        var omega5= omega4*omega;
        
        if (m == 1)
        {
            const mya = 14.83593084
            const myb = -6.96298673
            const myc = 22.20188339
            const myd = 468.7358133
            const mye = 7220.95995
            const myf = -983.603764
            const myg = 23193.47496
            const myh = 914.3607833
            const myi = -6207.46635
            const myj = -1383.44414
            const myk = 1732.959956
            
            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3+myi*omega4+myk*omega5)/(1+myb*omega+myd*omega2+myf*omega3+myh*omega4+myj*omega5);
            
            const mya = 0.128418694
            const myb = 0.404864417
            const myc = 1.677162694
            const myd = -0.15919454
            
            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));

            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;

            var epsilon = Tc/((mya+myc*alpha)/(1+myb*alpha+myd*alpha*alpha));
            
            const mya = 1.896593618
            const myb = -1.62045652
            const myc = -6.98084869
            const myd = -0.80187118
            const mye = 10.63303009
            const myf = 1.708603778
            const myg = -9.20413054
            const myh = -0.53329899
            const myi = 4.250332513
            const myj = 1.053581249
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3+myi*alpha4)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4+myj*alpha5);
            
            var RAKKon = document.getElementById('mycheck').checked
            
            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }
            
            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
                        
            mytextarea.value= "For m = 1; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
                              "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
                              "\n"+"epsilon/KB = "+epsilon+" [K]"+
                              "\n"+"sigma = "+sigma+" [m]";
        }
        else if (m == 2)
        {
            const mya = 8.003422132
            const myb = -5.26687379
            const myc = -22.511103
            const myd = 10.22988602
            const mye = 3.575024001
            const myf = -6.48604227
            const myg = 60.3129302
            
            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3)/(1+myb*omega+myd*omega2+myf*omega3);
            
            const mya = 0.112471398
            const myb = -3.19537
            const myc = 1.540433976
            const myd = 2.517383739
            const mye = -5.87688084
            const myf = 0.35178742
            const myg = 5.242694662
            const myh = -0.16540502
            
            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));
            
            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;
            
            var epsilon = Tc/((mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4));
            
            const mya = -0.06956046
            const myb = -10.5645885
            const myc = -1.94405528
            const myd = 25.49144986
            const mye = 6.257546817
            const myf = -20.5091442
            const myg = -5.44310717
            const myh = 3.67529192
            const myi = 0.873127484
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3+myi*alpha4)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4);

            var RAKKon = document.getElementById('mycheck').checked
            
            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }
            
            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
            
            mytextarea.value= "For m = 2; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
            "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
            "\n"+"epsilon/KB = "+epsilon+" [K]"+
            "\n"+"sigma = "+sigma+" [m]";
        }
        else if (m == 3)
        {
            const mya = 6.98288283
            const myb = -3.86904975
            const myc = -13.7097069
            const myd = 5.251856687
            const mye = -1.96037436
            const myf = -2.36371109
            const myg = 17.32374946

            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3)/(1+myb*omega+myd*omega2+myf*omega3);
            
            const mya = -0.20924076
            const myb = -1.37777443
            const myc = 4.26721507
            const myd = -2.48364057
            const mye = -9.77031277
            const myf = 3.528008069
            const myg = 4.866147232
            const myh = 0.791774193
            const myi = -0.19503132
            const myj = -0.12464336
            const myk = 4.212468181


            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));
            
            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;
            
            var epsilon = Tc/((mya+myc*alpha+mye*alpha2+myg*alpha3+myi*alpha4+myk*alpha5)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4+myj*alpha5));
            
            const mya = 0.06560659
            const myb = -8.93086313
            const myc = -1.46302137
            const myd = 18.9584471
            const mye = 3.699140072
            const myf = -11.6668281
            const myg = -2.5080772
            const myh = -0.25609451
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4);

            var RAKKon = document.getElementById('mycheck').checked

            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }

            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
            
            mytextarea.value= "For m = 3; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
            "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
            "\n"+"epsilon/KB = "+epsilon+" [K]"+
            "\n"+"sigma = "+sigma+" [m]";
        }
        else if (m == 4)
        {
            const mya = 6.415946724
            const myb = -6.97513187
            const myc = -34.3656175
            const myd = 19.20628034
            const mye = 59.61079835
            const myf = -26.0582818
            const myg = -21.6578918
            const myh = 17.42217972
            const myi = -35.8209965
            const myj = -4.57567105
            const myk = 27.23582702
            
            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3+myi*omega4+myk*omega5)/(1+myb*omega+myd*omega2+myf*omega3+myh*omega4+myj*omega5);
            
            const mya = 0.135041681
            const myb = -5.85398723
            const myc = 1.311532849
            const myd = 13.34114852
            const mye = -10.1436616
            const myf = -14.3301568
            const myg = 24.07289203
            const myh = 6.730897463
            const myi = -24.808427
            const myj = -0.7830166
            const myk = 9.710951926
            
            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));
            
            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;
            
            var epsilon = Tc/((mya+myc*alpha+mye*alpha2+myg*alpha3+myi*alpha4+myk*alpha5)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4+myj*alpha5));
            
            const mya = 0.10252404
            const myb = -8.10766945
            const myc = -1.19478912
            const myd = 16.786554
            const mye = 2.844771765
            const myf = -9.63544898
            const myg = -1.95195179
            const myh = -1.23903092
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4);

            var RAKKon = document.getElementById('mycheck').checked
            
            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }
            
            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
            
            mytextarea.value= "For m = 4; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
            "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
            "\n"+"epsilon/KB = "+epsilon+" [K]"+
            "\n"+"sigma = "+sigma+" [m]";
        }
        else if (m == 5)
        {
            const mya = 6.12835725
            const myb = -2.84856149
            const myc = -9.15683798
            const myd = 2.782770909
            const mye = -0.22289564
            const myf = -0.90298914
            const myg = 4.5310789
            
            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3)/(1+myb*omega+myd*omega2+myf*omega3);
            
            const mya = 0.110713117
            const myb = -3.13411835
            const myc = 1.980666037
            const myd = 2.765748576
            const mye = -6.67199531
            const myf = -0.27372772
            const myg = 5.484068126
            const myh = -0.043068
            
            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));
            
            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;
            
            var epsilon = Tc/((mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4));
            
            const mya = 0.110777611
            const myb = -7.37489698
            const myc = -0.98996221
            const myd = 14.53126538
            const mye = 2.218722062
            const myf = -7.49666538
            const myg = -1.50270742
            const myh = -1.92091089
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3+myh*alpha4);
            
            var RAKKon = document.getElementById('mycheck').checked
            
            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }
            
            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
            
            mytextarea.value= "For m = 5; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
            "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
            "\n"+"epsilon/KB = "+epsilon+" [K]"+
            "\n"+"sigma = "+sigma+" [m]";
        }
        else if (m == 6)
        {
            const mya = 5.921678461
            const myb = -2.52907417
            const myc = -8.07106878
            const myd = 2.186427641
            const mye = 0.426376247
            const myf = -0.62977458
            const myg = 2.559960841
            
            var lambda_r = (mya+myc*omega+mye*omega2+myg*omega3)/(1+myb*omega+myd*omega2+myf*omega3);
            
            const mya = 0.130226302
            const myb = -3.10782289
            const myc = 1.93570739
            const myd = 2.805801744
            const mye = -6.45905649
            const myf = -0.43748638
            const myg = 5.186407719
            
            var CC = (lambda_r/(lambda_r-6))* Math.pow((lambda_r/6),((6/(lambda_r-6))));
            
            var alpha  = CC*((1/(3))-(1/(lambda_r-3)));
            var alpha2 = alpha*alpha;
            var alpha3 = alpha2*alpha;
            var alpha4 = alpha3*alpha;
            var alpha5 = alpha4*alpha;
            
            var epsilon = Tc/((mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3));
            
            const mya = 0.266554267
            const myb = 1.7499403
            const myc = -0.42677989
            const myd = -10.1370339
            const mye = -0.27319773
            const myf = 9.438065562
            const myg = 0.648629893
            
            var M21   = (mya+myc*alpha+mye*alpha2+myg*alpha3)/(1+myb*alpha+myd*alpha2+myf*alpha3);
            
            var RAKKon = document.getElementById('mycheck').checked
            
            if (RAKKon == true)
            {
                var rho07 = rhoc*Math.pow(8.31446E-05*Tc*rhoc/Pc,0.708934437);
            }
            
            var sigma = Math.pow(M21/(rho07*6.0221367E+23),(1/3));
            
            mytextarea.value= "For m = 6; Tc= "+Tc+"; Pc= "+Pc+"; rhoc= "+rhoc+"; omega= "+omega+"; rho @Tr0.7 = "+rho07+
            "\n"+"lambda_a = 6; lambda_r = "+lambda_r+
            "\n"+"epsilon/KB = "+epsilon+" [K]"+
            "\n"+"sigma = "+sigma+" [m]";
        }
       else
        {
            mytextarea.value= "Sorry, not correlation for m = "+m;
        }
        
    }
